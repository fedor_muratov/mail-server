package server_interaction;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import proto.client_interaction.MailSenderGrpc;
import proto.health_check.HealthCheckGrpc;

public class ClientStub {
    private final String host;
    private final int port;
    private MailSenderGrpc.MailSenderBlockingStub mailApiStub;
    private HealthCheckGrpc.HealthCheckBlockingStub healthCheckApiStub;

    public ClientStub(String host, int port) {
        this.host = host;
        this.port = port;
    }

    public MailSenderGrpc.MailSenderBlockingStub mailApiStub() {
        return mailApiStub;
    }

    public HealthCheckGrpc.HealthCheckBlockingStub healthCheckApiStub() {
        return healthCheckApiStub;
    }

    public void initClient() {
        ManagedChannelBuilder<?> channelBuilder = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext();
        ManagedChannel channel = channelBuilder.build();
        mailApiStub = MailSenderGrpc.newBlockingStub(channel);
        healthCheckApiStub = HealthCheckGrpc.newBlockingStub(channel);
    }
}
