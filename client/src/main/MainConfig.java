package main;

import mail.data.nested.RequestProperties;
import picocli.CommandLine;
import util.StringFormatter;

@CommandLine.Command()
public class MainConfig {


    @CommandLine.Option(names = {"-host"}, description = "Host of the server", required = true)
    private String host;
    @CommandLine.Option(names = {"-port"}, description = "Port of the server", required = true)
    private int port;
    @CommandLine.ArgGroup(exclusive = false)
    private MailRequest mailRequest;

    public String host() {
        return host;
    }

    public int port() {
        return port;
    }

    public MailRequest mailRequest() {
        return mailRequest;
    }

    @Override
    public String toString() {
        return new StringFormatter("ServerPreferences")
                .append("host", host)
                .append("port", port)
                .append("mailRequest", mailRequest)
                .toString();
    }

    public static class MailRequest {

        @CommandLine.Option(names = {"-async"}, required = true, description = "Should CLI wait of the response")
        private boolean async = false;

        @CommandLine.Option(names = {"-request"}, required = true, description = "Type of the request")
        private RequestProperties.SendingStrategy strategy = RequestProperties.SendingStrategy.RELIABLE;

        @Override
        public String toString() {
            return new StringFormatter("SendPreferences").append("async", async)
                    .append("strategy", strategy.name())
                    .toString();
        }

        public boolean isAsync() {
            return async;
        }

        public RequestProperties.SendingStrategy strategy() {
            return strategy;
        }
    }
}
