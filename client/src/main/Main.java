package main;

import com.google.protobuf.Empty;
import health_check.PongResponse;
import health_check.converters.ProtoPongResponseConverter;
import io.grpc.StatusRuntimeException;
import mail.data.SendMailRequest;
import mail.data.converters.ConversionException;
import mail.data.converters.proto.RequestProtoConverter;
import mail.data.nested.Content;
import mail.data.nested.Mail;
import mail.data.nested.MailAddress;
import mail.data.nested.RequestProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import picocli.CommandLine;
import proto.client_interaction.MailApi;
import proto.health_check.HealthcheckApi;
import server_interaction.ClientStub;
import util.Result;

public class Main {
    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {

        Main main = new Main();
        MainConfig mainConfig = main.parseCommandLine(args);
        if (mainConfig == null) {
            return;
        }
        log.info("Parsed arguments: {}", mainConfig);

        try {
            ClientStub stub = new ClientStub(mainConfig.host(), mainConfig.port());
            stub.initClient();
            if (mainConfig.mailRequest() == null) {
                main.requestPing(stub);
            } else {
                log.info("Send mail request");
                RequestProtoConverter converter = new RequestProtoConverter();
                SendMailRequest request = new SendMailRequest.RequestBuilder(getDefaultMail())
                        .setProperties(new RequestProperties(mainConfig.mailRequest().strategy()))
                        .build();
                converter.convert(request)
                        .visitVoid(protoRequest -> invokeMailSending(mainConfig,
                                stub,
                                protoRequest),
                                err -> log.warn("Conversion to proto had failed", err));

            }
        } catch (StatusRuntimeException ex) {
            log.error("RPC request failed: {}", ex.getStatus());
        }

    }

    private static void invokeMailSending(MainConfig mainConfig,
                                          ClientStub stub,
                                          MailApi.ClientSendMailRequest protoRequest) {
        if (mainConfig.mailRequest().isAsync()) {
            log.info("Send mail using async method");
            stub.mailApiStub().sendMailAsync(protoRequest);
        } else {
            log.info("Send mail using blocking method");
            MailApi.ClientSendMailResponse response = stub.mailApiStub()
                    .sendMail(protoRequest);
            // TODO: rework with response converter
            log.info("Server response: {} with explanation {}",
                    response.getStatus(),
                    response.getExplanation());
        }
    }

    private static Mail getDefaultMail() {
        return new Mail.MailBuilder(MailAddress.create("foo@example.com").get(),
                MailAddress.create("baz@test.org").get())
                .setContent(Content.empty()).setSubject("Client send a request").build();
    }

    private void requestPing(ClientStub stub) {
        ProtoPongResponseConverter converter = new ProtoPongResponseConverter();
        log.info("Send ping");
        HealthcheckApi.PongResponse response = stub.healthCheckApiStub()
                .ping(Empty.newBuilder().build());
        Result<PongResponse, ConversionException> converted = converter.convert(response);
        converted.visitVoid(v -> log.info("Response: {}", v),
                e -> log.warn("Conversion to proto had failed", e));
    }

    private MainConfig parseCommandLine(String[] args) {
        try {
            return CommandLine.populateCommand(new MainConfig(), args);
        } catch (Exception ex) {
            log.error("Failed to parse command line: {}", ex.getMessage());
            return null;
        }
    }
}
