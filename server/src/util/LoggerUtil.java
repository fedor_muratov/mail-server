package util;

public class LoggerUtil {
    /**
     * Identifier of performance marker
     */
    public static String PERFORMANCE_MARKER = "Performance";

    /**
     * Identifies troubles with responses to clients due to network issues
     */
    public static String NETWORK_ERROR_MARKER = "NetworkError";

}
