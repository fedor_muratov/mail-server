package mail_service_interaction.mail_service_providers;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Wrapper for services which may fall during the work
 *
 * @param <Service> - type of the service
 */
public abstract class MailServiceReconnectionWrapper<Service> {

    private Service service;

    private final ReadWriteLock lock;
    private final Lock readLock;
    private final Lock writeLock;

    protected MailServiceReconnectionWrapper() {
        lock = new ReentrantReadWriteLock();
        readLock = lock.readLock();
        writeLock = lock.writeLock();
    }

    /**
     * Method for initialization/reinitialization of service
     *
     * @return initialized service
     */
    protected abstract Service initializeService();

    /**
     * Method provide blocking reconnection to the service
     */
    public void reconnect() {
        // TODO: prevent double reconnection
        writeLock.lock();
        try {
            service = initializeService();
        } finally {
            writeLock.unlock();
        }
    }

    /**
     * Getter for the service reference.
     * Note: the method is blocking
     */
    public Service getMailService() {
        readLock.lock();
        try {
            return service;
        } finally {
            readLock.unlock();
        }
    }
}
