package mail_service_interaction.mail_service_providers.sendgrid;

import com.sendgrid.helpers.mail.objects.Personalization;
import mail.data.nested.Mail;

public class SendGridConverterBundle {
    private com.sendgrid.helpers.mail.Mail targetMail;
    private Personalization                personalization;
    private final Mail sourceMail;

    public SendGridConverterBundle(Mail sourceMail) {
        this.sourceMail = sourceMail;
        targetMail = new com.sendgrid.helpers.mail.Mail();
        personalization = new Personalization();
    }

    public com.sendgrid.helpers.mail.Mail targetMail() {
        return targetMail;
    }

    public Personalization personalization() {
        return personalization;
    }

    public Mail sourceMail() {
        return sourceMail;
    }
}
