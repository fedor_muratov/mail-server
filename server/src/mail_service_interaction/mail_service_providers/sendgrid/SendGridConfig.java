package mail_service_interaction.mail_service_providers.sendgrid;

import util.Result;

/**
 * Class responsible for holding preferences for interaction with SendGrid
 */
public class SendGridConfig {
    private final String apiKey;

    /**
     * Environment variable with the API key
     */
    public static final String ENV_KEY = "SENDGRID_API_KEY";

    private SendGridConfig(String apiKey) {
        this.apiKey = apiKey;
    }

    public String revealApiKey() {
        return apiKey;
    }

    /**
     * Create a config which retrieve parameters from environment variables
     */
    public static Result<SendGridConfig, String> getConfig() {
        String key = System.getenv(ENV_KEY);
        if (key == null || key.equals("")) {
            return Result.fromError("missed SendGrid key");
        }
        return Result.fromValue(new SendGridConfig(key));
    }
}
