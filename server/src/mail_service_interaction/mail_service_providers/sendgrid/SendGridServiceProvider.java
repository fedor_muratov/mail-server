package mail_service_interaction.mail_service_providers.sendgrid;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;
import mail.data.MailProviderResponse;
import mail.data.converters.ConversionException;
import mail.data.converters.MailConverter;
import mail.data.nested.Mail;
import mail_service_interaction.MailServiceProvider;
import mail_service_interaction.mail_service_providers.MailServiceReconnectionWrapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.MarkerManager;
import proto.client_interaction.MailApi;
import util.LoggerUtil;
import util.Result;

import java.io.IOException;

public class SendGridServiceProvider implements MailServiceProvider {

    private static final Logger log = LogManager.getLogger(SendGridServiceProvider.class);

    private MailConverter<Mail, com.sendgrid.helpers.mail.Mail> converter;
    private MailServiceReconnectionWrapper<SendGrid> service;

    public SendGridServiceProvider(MailServiceReconnectionWrapper<SendGrid> service,
                                   MailConverter<Mail, com.sendgrid.helpers.mail.Mail> converter) {
        this.converter = converter;
        this.service = service;
    }

    @Override
    public MailProviderResponse send(Mail mail) {
        Result<com.sendgrid.helpers.mail.Mail, ConversionException> result = converter.convert(mail);

        return result.visit(this::syncSend,
                conversionError -> MailProviderResponse.malformedRequest(conversionError.getReason()));
    }

    @Override
    public String getServiceName() {
        return MailApi.RequestProperties.SendStrategy.SEND_GRID.name();
    }

    private MailProviderResponse syncSend(com.sendgrid.helpers.mail.Mail mail) {
        try {
            SendGrid mailService = service.getMailService();
            if (mailService == null) {
                log.warn("SendGrid service is absent");
                service.reconnect();
                return MailProviderResponse.mailProviderProblem(
                        "Null: connection to SendGrid is fallen");
            }
            Request request = new Request();
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(mail.build());
            Response response = mailService.api(request);
            if (response.getStatusCode() != 202) {
                log.warn("Status: {}", response.getStatusCode());
                log.warn("Body: {}", response.getBody());
                log.warn("Headers: {}", response.getHeaders());
                log.warn(MarkerManager.getMarker(LoggerUtil.NETWORK_ERROR_MARKER),
                        "sending has finished with unexpected status: {}",
                        response.getStatusCode());
            }
        } catch (IOException ex) {
            log.warn(MarkerManager.getMarker(LoggerUtil.NETWORK_ERROR_MARKER),
                    "Problems with the connection are occurred", ex);
            service.reconnect();
            return MailProviderResponse.mailProviderProblem("IO: connection to SendGrid is fallen");
        }

        return MailProviderResponse.success();
    }
}
