package mail_service_interaction.mail_service_providers.sendgrid;

import com.sendgrid.SendGrid;
import mail_service_interaction.mail_service_providers.MailServiceReconnectionWrapper;

public class SendGridReconnectionWrapper extends MailServiceReconnectionWrapper<SendGrid> {

    private final String apiKey;

    public SendGridReconnectionWrapper(String apiKey) {
        super();
        this.apiKey = apiKey;
        reconnect();
    }

    @Override
    protected SendGrid initializeService() {
        return new SendGrid(apiKey);
    }
}
