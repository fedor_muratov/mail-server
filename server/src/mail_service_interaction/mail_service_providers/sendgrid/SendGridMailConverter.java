package mail_service_interaction.mail_service_providers.sendgrid;

import com.sendgrid.helpers.mail.Mail;
import com.sendgrid.helpers.mail.objects.Email;
import mail.data.converters.ConversionException;
import mail.data.converters.MailConverter;
import mail.data.nested.Content;
import mail.data.nested.MailAddress;
import util.NotNull;

public class SendGridMailConverter extends MailConverter<mail.data.nested.Mail, Mail> {

    private SendGridConverterBundle bundle;

    @Override
    protected void initialize(mail.data.nested.Mail mail) throws ConversionException {
        NotNull.check(mail, "Mail");
        bundle = new SendGridConverterBundle(mail);
    }

    @Override
    protected void fetchFrom() throws ConversionException {
        bundle.targetMail().from = new Email(NotNull.fetchNotNull(() -> bundle.sourceMail()
                .from()
                .plainAddress(), "Mail::From"));
    }

    @Override
    protected void fetchTo() throws ConversionException {
        bundle.personalization()
                .addTo(new Email(NotNull.fetchNotNull(() -> bundle.sourceMail().to().plainAddress(),
                        "Mail::To")));
    }

    @Override
    protected void fetchCopyTo() throws ConversionException {
        for (MailAddress mailAddress : NotNull.fetchNotNull(() -> bundle.sourceMail().сс(),
                "Mail::CC collection")) {
            bundle.personalization()
                    .addCc(NotNull.fetchNotNull(() -> new Email(mailAddress.plainAddress()),
                            "Mail::CC mail"));
        }
    }

    @Override
    protected void fetchContent() throws ConversionException {
        String contentType = null;
        // TODO: switch by all types
        if (NotNull.fetchNotNull(() -> bundle.sourceMail().content().type(),
                "Mail::Content::Type") == Content.ContentType.PLAIN_TEXT) {
            contentType = "text/plain";
        }
        bundle.targetMail()
                .addContent(new com.sendgrid.helpers.mail.objects.Content(contentType,
                        NotNull.fetchNotNull(() -> bundle.sourceMail().content().data(),
                                "Mail::Content::Data")));
    }

    @Override
    protected void fetchSubject() throws ConversionException {
        bundle.targetMail()
                .setSubject(NotNull.fetchNotNull(() -> bundle.sourceMail().subject(),
                        "Mail::Subject"));
    }

    @Override
    protected Mail create() {
        bundle.targetMail().addPersonalization(bundle.personalization());
        return bundle.targetMail();
    }

}
