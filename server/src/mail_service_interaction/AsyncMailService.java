package mail_service_interaction;

import mail.data.MailProviderResponse;
import mail.data.nested.Mail;
import mail.data.nested.RequestProperties;

import java.util.concurrent.Future;

/**
 * The interface provides unblocking interaction with mail server
 */
public interface AsyncMailService {

    /**
     * Method sends mail using provided strategy
     *
     * @param mail     - target mail for sending
     * @param strategy - method for sending
     * @return Future with a result of sending
     */
    Future<MailProviderResponse> send(Mail mail,
                                      RequestProperties.SendingStrategy strategy);
}
