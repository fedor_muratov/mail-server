package mail_service_interaction;

import mail.data.MailProviderResponse;
import mail.data.nested.Mail;
import mail.data.nested.RequestProperties;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.Formatter;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;

public class ConcurrentMailSender implements AsyncMailService {
    private static final Logger log = LogManager.getLogger(ConcurrentMailSender.class);

    private ServicePicker servicePicker;
    private ThreadPoolExecutor threadPool;


    public ConcurrentMailSender(ServicePicker servicePicker,
                                ThreadPoolExecutor threadPool) {
        this.servicePicker = servicePicker;
        this.threadPool = threadPool;
    }

    private static MailProviderResponse generateUnavailable(RequestProperties.SendingStrategy strategy) {
        return MailProviderResponse.mailProviderProblem(new Formatter().format(
                "Service %s is not available",
                strategy.name()).toString());
    }

    @Override
    public Future<MailProviderResponse> send(Mail mail,
                                             RequestProperties.SendingStrategy strategy) {
        return threadPool.submit(() -> {
            MailProviderResponse finalResponse = tryToSend(mail, strategy);
            log.info("Final client status: {}", finalResponse);
            return finalResponse;
        });
    }

    /**
     * Try to send mail with provided strategy
     *
     * @return final client's status
     */
    private MailProviderResponse tryToSend(Mail mail,
                                           RequestProperties.SendingStrategy strategy) {
        ServicePickerStrategy sendingStrategy = servicePicker.getStrategy(strategy);

        // branch means there will be no reconnection
        if (sendingStrategy == null) {
            return generateUnavailable(strategy);
        }
        Iterable<MailServiceProvider> orderOfServices = sendingStrategy.getNext();

        List<MailProviderResponse> previousResponses = new ArrayList<>();
        for (MailServiceProvider service : orderOfServices) {
            MailProviderResponse lastResponse = sendAttempt(service, mail);
            previousResponses.add(lastResponse);
            if (lastResponse.isSuccessful()) {
                break;
            }
        }
        return generateResponseFromTries(previousResponses);
    }

    /**
     * Method provide one attempt to send a mail
     *
     * @param provider - provider which will send a mail
     * @param mail     - target mail
     * @return status of the sending
     */
    private MailProviderResponse sendAttempt(MailServiceProvider provider,
                                             Mail mail) {
        log.trace("try to send mail {} using [{}]", mail, provider.getServiceName());
        return provider.send(mail);
    }

    private MailProviderResponse generateResponseFromTries(List<MailProviderResponse> providerResponses) {
        MailProviderResponse lastResponse = providerResponses.get(providerResponses.size() - 1);
        if (lastResponse.isSuccessful()) {
            return lastResponse;
        }
        if (providerResponses.size() == 1) {
            return providerResponses.get(0);
        } else {
            return generateUnavailableWithManyAttempts(providerResponses);
        }
    }

    private MailProviderResponse generateUnavailableWithManyAttempts(List<MailProviderResponse> errorResponses) {
        return MailProviderResponse.mailProviderProblem(new Formatter().format(
                "Number of sending attempts %d, but all have failed", errorResponses.size())
                .toString());
    }
}
