package mail_service_interaction;

/**
 * Interface provides ordered list of services
 */
public interface ServicePickerStrategy {
    /**
     * @return iterable list of services provided by the strategy
     */
    Iterable<MailServiceProvider> getNext();
}
