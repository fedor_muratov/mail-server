package mail_service_interaction.server_picker_strategies;

import mail_service_interaction.MailServiceProvider;
import mail_service_interaction.ServicePickerStrategy;

import java.util.LinkedList;
import java.util.List;

/**
 * Reliable strategy provides a full list of available mail services for sending
 */
public class ReliableStrategy implements ServicePickerStrategy {
    private final List<MailServiceProvider> providers;

    public ReliableStrategy(List<MailServiceProvider> providers) {
        this.providers = providers;
    }

    public Iterable<MailServiceProvider> getNext() {
        return new LinkedList<>(providers);
    }
}
