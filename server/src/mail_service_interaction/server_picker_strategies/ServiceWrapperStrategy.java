package mail_service_interaction.server_picker_strategies;

import mail_service_interaction.MailServiceProvider;
import mail_service_interaction.ServicePickerStrategy;

import java.util.ArrayList;
import java.util.List;

/**
 * Strategy provides fixed mail service for all time
 */
public class ServiceWrapperStrategy implements ServicePickerStrategy {
    private final List<MailServiceProvider> provider;

    public ServiceWrapperStrategy(MailServiceProvider provider) {
        this.provider = new ArrayList<>();
        this.provider.add(provider);
    }

    @Override
    public Iterable<MailServiceProvider> getNext() {
        return provider;
    }
}
