package mail_service_interaction.server_picker_strategies;

import mail_service_interaction.MailServiceProvider;
import mail_service_interaction.ServicePickerStrategy;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Provides a strategy which picks random service for sending
 */
public class AnyStrategy implements ServicePickerStrategy {
    private final List<MailServiceProvider> providers;
    private final Random rng;

    public AnyStrategy(List<MailServiceProvider> providers, Random random) {
        this.providers = providers;
        rng = random;
    }

    public Iterable<MailServiceProvider> getNext() {
        int i = rng.nextInt(providers.size());
        List<MailServiceProvider> list = new LinkedList<>();
        list.add(providers.get(i));
        return list;
    }
}
