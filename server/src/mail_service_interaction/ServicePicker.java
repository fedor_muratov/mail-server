package mail_service_interaction;

import mail.data.nested.RequestProperties;

import java.util.concurrent.ConcurrentHashMap;

/**
 * Class provides strategies mapped according to {@link RequestProperties.SendingStrategy}
 */
public class ServicePicker {

    private ConcurrentHashMap<RequestProperties.SendingStrategy, ServicePickerStrategy> availableProviders;

    public ServicePicker() {
        availableProviders = new ConcurrentHashMap<>();
    }

    /**
     * Get strategy by key
     *
     * @param key - identifier of the strategy
     * @return strategy if present, null otherwise
     */
    public ServicePickerStrategy getStrategy(RequestProperties.SendingStrategy key) {
        return availableProviders.get(key);
    }

    /**
     * Bind the strategy with a key
     */
    public void registerStrategy(RequestProperties.SendingStrategy key,
                                 ServicePickerStrategy service) {
        availableProviders.put(key, service);
    }
}
