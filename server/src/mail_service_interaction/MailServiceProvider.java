package mail_service_interaction;

import mail.data.MailProviderResponse;
import mail.data.nested.Mail;

/**
 * Interface of the target mail service
 */
public interface MailServiceProvider {

    /**
     * Blocking send mail via the mail service
     *
     * @param mail - mail for sending
     * @return client's response about the operation
     */
    MailProviderResponse send(Mail mail);

    /**
     * Name of the service
     */
    String getServiceName();
}
