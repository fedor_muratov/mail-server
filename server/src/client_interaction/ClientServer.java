package client_interaction;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Classs provides a wrapper for GRPC service
 */
public class ClientServer {

    private static final Logger log = LogManager.getLogger(ClientServer.class);

    private final Server grpcServer;

    private ClientServer(Server grpcServer) {
        this.grpcServer = grpcServer;
    }

    public void run() throws IOException {
        grpcServer.start();
        log.info("Server has started on port: {}", grpcServer.getPort());

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            ClientServer.this.shutdown();
            System.err.println("*** Client server shut down due to JVM termination");
        }));
    }

    public void await() throws InterruptedException {
        if (grpcServer != null) {
            grpcServer.awaitTermination();
        }
    }

    /**
     * Initialize the server with server builder
     */
    public static ClientServer create(ServerBuilder<?> builder) {
        return new ClientServer(builder.build());
    }

    public void shutdown() {
        if (grpcServer != null) {
            log.error("shutdown server");
            grpcServer.shutdown();
        }
    }
}
