package client_interaction;

import com.google.protobuf.Empty;
import io.grpc.stub.StreamObserver;
import mail.data.MailProviderResponse;
import mail.data.SendMailRequest;
import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import mail_service_interaction.AsyncMailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.MarkerManager;
import proto.client_interaction.MailApi;
import proto.client_interaction.MailSenderGrpc;
import util.LoggerUtil;
import util.Result;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.function.BiConsumer;

/**
 * GRPC service for client interaction
 */
public class ClientService extends MailSenderGrpc.MailSenderImplBase {

    private static final Logger log = LogManager.getLogger(ClientService.class);

    private Converter<MailApi.ClientSendMailRequest, SendMailRequest> requestConverter;
    private Converter<MailProviderResponse, MailApi.ClientSendMailResponse> responseConverter;
    private AsyncMailService asyncMailService;

    public ClientService(Converter<MailApi.ClientSendMailRequest, SendMailRequest> requestConverter,
                         Converter<MailProviderResponse, MailApi.ClientSendMailResponse> responseConverter,
                         AsyncMailService asyncMailService) {
        this.requestConverter = requestConverter;
        this.responseConverter = responseConverter;
        this.asyncMailService = asyncMailService;
    }


    @Override
    public void sendMail(MailApi.ClientSendMailRequest request,
                         StreamObserver<MailApi.ClientSendMailResponse> responseObserver) {
        performRequest(request, responseObserver, this::answerForClient);
    }

    @Override
    public void sendMailAsync(MailApi.ClientSendMailRequest request,
                              StreamObserver<Empty> responseObserver) {
        performRequest(request, responseObserver, (emptyStreamObserver, future) -> {
        });
    }

    /**
     * The method provides blocking answer for the client
     *
     * @param responseObserver - grpc observer with responses
     * @param invocation       - future with sending result
     */
    private void answerForClient(StreamObserver<MailApi.ClientSendMailResponse> responseObserver,
                                 Future<MailProviderResponse> invocation) {
        MailProviderResponse clientResponse = open(invocation);
        responseObserver.onNext(convertResponse(clientResponse));
    }

    private MailProviderResponse open(Future<MailProviderResponse> future) {
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            log.warn(LoggerUtil.NETWORK_ERROR_MARKER,
                    "troubles with getting the result of execution", e);
            return MailProviderResponse.mailProviderProblem(
                    "Server didn't send your email due to connection problems");
        }
    }

    /**
     * Generalization of request handling of client request
     *
     * @param request          - client's intention
     * @param responseObserver - grpc observer with responses
     * @param answer           - function which computes the response for the client
     * @param <T>              - data type of client's response
     */
    private <T> void performRequest(MailApi.ClientSendMailRequest request,
                                    StreamObserver<T> responseObserver,
                                    BiConsumer<StreamObserver<T>, Future<MailProviderResponse>> answer) {
        log.trace(MarkerManager.getMarker(LoggerUtil.PERFORMANCE_MARKER),
                "performRequest::OnStart, {}",
                request.hashCode());
        try {
            Future<MailProviderResponse> invocation = convertRequest(request).visit(this::onWellFormedRequest,
                    this::onMalformedRequest);
            log.trace(MarkerManager.getMarker(LoggerUtil.PERFORMANCE_MARKER),
                    "performRequest::onEndConversion, {}",
                    request.hashCode());
            answer.accept(responseObserver, invocation);
        } catch (Exception ex) {
            log.error("Uncaught exception in performRequest", ex);
        } finally {
            responseObserver.onCompleted();
        }
        log.trace(MarkerManager.getMarker(LoggerUtil.PERFORMANCE_MARKER),
                "performRequest::onEnd, {}",
                request.hashCode());
    }

    private Result<SendMailRequest, ConversionException> convertRequest(MailApi.ClientSendMailRequest protoRequest) {
        return requestConverter.convert(protoRequest);
    }

    private Future<MailProviderResponse> onWellFormedRequest(SendMailRequest sendMailRequest) {
        return asyncMailService.send(sendMailRequest.mail(),
                sendMailRequest.properties().service());

    }

    private Future<MailProviderResponse> onMalformedRequest(ConversionException ex) {
        return CompletableFuture.completedFuture(MailProviderResponse.malformedRequest(ex.getReason()));
    }

    private MailApi.ClientSendMailResponse convertResponse(MailProviderResponse serverResponse) {
        return responseConverter.convert(serverResponse).visit(protoResponse -> protoResponse);
    }
}
