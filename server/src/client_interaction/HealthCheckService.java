package client_interaction;

import com.google.protobuf.Empty;
import health_check.PongResponse;
import io.grpc.stub.StreamObserver;
import mail.data.converters.SafeConverter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import proto.health_check.HealthcheckApi;

import java.time.Instant;

public class HealthCheckService extends proto.health_check.HealthCheckGrpc.HealthCheckImplBase {

    private static final Logger log = LogManager.getLogger(HealthCheckService.class);

    private final SafeConverter<PongResponse, HealthcheckApi.PongResponse> converter;
    private final long startTime;

    public HealthCheckService(SafeConverter<PongResponse, HealthcheckApi.PongResponse> converter,
                              long startTime) {
        this.converter = converter;
        this.startTime = startTime;
    }

    @Override
    public void ping(Empty request, StreamObserver<HealthcheckApi.PongResponse> responseObserver) {
        log.info("Ping request");
        responseObserver.onNext(converter.convert(pong()));
        responseObserver.onCompleted();
    }

    private PongResponse pong() {
        long diff = Instant.now().getEpochSecond() - startTime;
        return new PongResponse(diff * 1000);
    }
}
