package main;

import client_interaction.ClientServer;
import client_interaction.ClientService;
import client_interaction.HealthCheckService;
import health_check.converters.PongResponseToProtoConverter;
import io.grpc.ServerBuilder;
import mail.data.converters.proto.ProtoRequestConverter;
import mail.data.converters.proto.ResponseProtoConverter;
import mail.data.nested.RequestProperties;
import mail_service_interaction.AsyncMailService;
import mail_service_interaction.ConcurrentMailSender;
import mail_service_interaction.MailServiceProvider;
import mail_service_interaction.ServicePicker;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridConfig;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridMailConverter;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridReconnectionWrapper;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridServiceProvider;
import mail_service_interaction.server_picker_strategies.AnyStrategy;
import mail_service_interaction.server_picker_strategies.ReliableStrategy;
import mail_service_interaction.server_picker_strategies.ServiceWrapperStrategy;
import org.apache.logging.log4j.Logger;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import static org.apache.logging.log4j.LogManager.getLogger;

public class ComponentsInitialization {

    private static final Logger log = getLogger(Main.class);

    private ServicePicker servicePicker;
    private AsyncMailService mailSender;

    private List<MailServiceProvider> availableServices;

    private ClientServer grpcServer;

    public ComponentsInitialization() {
        this.availableServices = new ArrayList<>();
        servicePicker = new ServicePicker();
    }

    public ClientServer init(int port) {
        initializeSendGrid();
        initializationChecks();
        inflateServiceProvider();

        initializeMailService();

        initializeGrpcServer(port);

        return grpcServer;
    }

    private void inflateServiceProvider() {
        servicePicker.registerStrategy(RequestProperties.SendingStrategy.ANY,
                new AnyStrategy(availableServices, new Random()));
        servicePicker.registerStrategy(RequestProperties.SendingStrategy.RELIABLE,
                new ReliableStrategy(availableServices));
    }

    private void initializeSendGrid() {
        log.info("initialize SendGrid service");
        SendGridConfig.getConfig().visitVoid(cfg -> {
            SendGridMailConverter converter = new SendGridMailConverter();
            SendGridServiceProvider sgService = new SendGridServiceProvider(new SendGridReconnectionWrapper(
                    cfg.revealApiKey()),
                    converter);
            servicePicker.registerStrategy(RequestProperties.SendingStrategy.SEND_GRID,
                    new ServiceWrapperStrategy(sgService));
            availableServices.add(sgService);
        }, err -> log.warn("SendGrid Service is not initialized: bad key. Did you set up {}?",
                SendGridConfig.ENV_KEY));
    }

    private void initializeMailService() {
        log.info("initialize mail service");
        ThreadPoolExecutor threadPool = (ThreadPoolExecutor) Executors.newFixedThreadPool(5);
        mailSender = new ConcurrentMailSender(servicePicker,
                threadPool);
    }

    private void initializeGrpcServer(int port) {
        ClientService clientApi = clientApi();
        HealthCheckService healthCheckApi = initializeHealthCheck();
        log.info("initialize Client GRPC server");
        ServerBuilder<?> builder = ServerBuilder.forPort(port)
                .addService(clientApi)
                .addService(healthCheckApi);
        grpcServer = ClientServer.create(builder);
    }

    private HealthCheckService initializeHealthCheck() {
        long epochSecond = Instant.now().getEpochSecond();
        log.info("The sever stared in {}", epochSecond);
        return new HealthCheckService(new PongResponseToProtoConverter(), epochSecond);
    }

    private ClientService clientApi() {
        log.info("Initialize client api");
        return new ClientService(new ProtoRequestConverter(),
                new ResponseProtoConverter(),
                mailSender);
    }

    private void initializationChecks() {
        log.info("initialization checks");
        if (availableServices.size() == 0) {
            log.error("Absent at least one mail provider for starting");
            if (grpcServer != null) {
                grpcServer.shutdown();
            }
            throw new RuntimeException("Should initialize at least service provider");
        }
    }
}
