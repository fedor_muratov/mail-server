package main;

import client_interaction.ClientServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class Main {

    private static final Logger log = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        ClientServer server = new ComponentsInitialization().init(0);
        try {
            server.run();
            server.await();
        } catch (InterruptedException | IOException ex) {
            log.error(ex);
        }
    }
}
