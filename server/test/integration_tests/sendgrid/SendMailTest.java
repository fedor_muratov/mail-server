package sendgrid;

import com.sendgrid.SendGrid;
import mail.data.MailProviderResponse;
import mail.data.nested.Content;
import mail.data.nested.Mail;
import mail.data.nested.MailAddress;
import mail_service_interaction.MailServiceProvider;
import mail_service_interaction.mail_service_providers.MailServiceReconnectionWrapper;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridConfig;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridMailConverter;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridReconnectionWrapper;
import mail_service_interaction.mail_service_providers.sendgrid.SendGridServiceProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;

public class SendMailTest {

    private static final Logger log = LogManager.getLogger(SendMailTest.class);

    private static MailServiceReconnectionWrapper<SendGrid> service;
    private MailServiceProvider mailServiceProvider;

    @BeforeClass
    public static void setUpSuite() {

        SendGridConfig config = SendGridConfig.getConfig().visit(conf -> conf,
                error -> {
                    log.error("Config didn't fetched: {}", error);
                    fail("config should be initialized");
                    return null;
                });

        service = new SendGridReconnectionWrapper(config.revealApiKey());
    }

    @Before
    public void setUp() {
        mailServiceProvider = new SendGridServiceProvider(service, new SendGridMailConverter());
    }

    /**
     * @given initialized SendGrid service
     * @when send a valid mail
     * @then check that mail has been sent
     */
    @Test
    public void testSendValidMail() {
        Mail mail = new Mail.MailBuilder(MailAddress.create("test@example.com").get(),
                MailAddress.create("test@test.com").get()).setContent(new Content(
                "and easy to do anywhere, even with Java",
                Content.ContentType.PLAIN_TEXT))
                .setSubject("Sending with Twilio SendGrid is Fun")
                .build();

        MailProviderResponse response = mailServiceProvider.send(mail);
        assertTrue(response.getErrorReason(), response.isSuccessful());
    }
}
