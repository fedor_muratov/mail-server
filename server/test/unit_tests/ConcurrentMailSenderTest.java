import mail.data.MailProviderResponse;
import mail.data.nested.Mail;
import mail.data.nested.RequestProperties;
import mail_service_interaction.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class ConcurrentMailSenderTest {


    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();
    @Mock
    ServicePickerStrategy mockServicePickerStrategy;
    @Mock
    private ThreadPoolExecutor mockThreadPool;
    @Mock
    private ServicePicker mockPicker;
    @Mock
    private MailServiceProvider mockMailServiceProvider;

    private AsyncMailService mailSender;

    @Before
    public void setUp() {
        mailSender = new ConcurrentMailSender(mockPicker, mockThreadPool);

        when(mockThreadPool.submit(any(Callable.class))).thenAnswer(i -> {
            Callable r = (Callable) i.getArguments()[0];
            return CompletableFuture.completedFuture(r.call());
        });
    }

    /**
     * @given Initialized {@link ConcurrentMailSender}
     * @when invoke {@link ConcurrentMailSender#send(Mail, RequestProperties.SendingStrategy)}
     * @and the service present
     * @and return success result
     * @then check that {@link MailServiceProvider} is in use
     */
    @Test
    public void testOnSendMailWithService() {
        when(mockPicker.getStrategy(RequestProperties.SendingStrategy.SEND_GRID)).thenReturn(
                mockServicePickerStrategy);
        when(mockServicePickerStrategy.getNext()).thenReturn(Arrays.asList(mockMailServiceProvider));
        when(mockMailServiceProvider.send(any(Mail.class))).thenReturn(MailProviderResponse.success());
        when(mockMailServiceProvider.getServiceName()).thenReturn("MockService");

        mailSender.send(mock(Mail.class), RequestProperties.SendingStrategy.SEND_GRID);

        verify(mockThreadPool, times(1)).submit(any(Callable.class));
        verify(mockServicePickerStrategy, times(1)).getNext();
        verify(mockMailServiceProvider, times(1)).send(any(Mail.class));
    }

    /**
     * @given Initialized {@link ConcurrentMailSender}
     * @when invoke {@link ConcurrentMailSender#send(Mail, RequestProperties.SendingStrategy)}
     * @and service doesn't present
     * @then check that {@link MailServiceProvider} isn't in use
     * @and return failure message
     */
    @Test
    public void testOnSendMailWithNullService() throws ExecutionException, InterruptedException {
        when(mockPicker.getStrategy(RequestProperties.SendingStrategy.SEND_GRID)).thenReturn(null);

        Assert.assertFalse(mailSender.send(mock(Mail.class),
                RequestProperties.SendingStrategy.SEND_GRID)
                .get()
                .isSuccessful());

        verify(mockThreadPool, times(1)).submit(any(Callable.class));
    }

    /**
     * @given Initialized {@link ConcurrentMailSender}
     * @when invoke {@link ConcurrentMailSender#send(Mail, RequestProperties.SendingStrategy)}
     * @and 3 services presented
     * @and first returns error
     * @and second returns success
     * @then check that 3rd service is not called
     */
    @Test
    public void testOnSendMailWithManyService() {
        when(mockPicker.getStrategy(RequestProperties.SendingStrategy.SEND_GRID)).thenReturn(
                mockServicePickerStrategy);

        MailServiceProvider successMockMailService = mock(MailServiceProvider.class);
        MailServiceProvider unUsedMockMailService = mock(MailServiceProvider.class);

        when(mockServicePickerStrategy.getNext()).thenReturn(
                Arrays.asList(mockMailServiceProvider,
                        successMockMailService,
                        unUsedMockMailService));

        when(mockMailServiceProvider.send(any(Mail.class))).thenReturn(MailProviderResponse.malformedRequest(
                ""));
        when(successMockMailService.send(any(Mail.class))).thenReturn(MailProviderResponse.success());

        when(mockMailServiceProvider.getServiceName()).thenReturn("MockService");

        mailSender.send(mock(Mail.class), RequestProperties.SendingStrategy.SEND_GRID);

        verify(mockThreadPool, times(1)).submit(any(Callable.class));
        verify(mockServicePickerStrategy, times(1)).getNext();
        verify(mockMailServiceProvider, times(1)).send(any(Mail.class));
        verify(successMockMailService, times(1)).send(any(Mail.class));
        verify(unUsedMockMailService, never()).send(any(Mail.class));

    }
}
