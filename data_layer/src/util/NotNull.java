package util;

import mail.data.converters.ConversionException;

import java.util.function.Supplier;

/**
 * Class contains methods for checking object with null value
 */
public class NotNull {
    public static <T> boolean test(T obj) {
        return obj != null;
    }

    public static <T> void check(T obj) throws ConversionException {
        check(obj, null);
    }

    public static <T> void check(T obj, String field) throws ConversionException {
        if (!test(obj)) {
            String error;
            if (field == null) {
                error = "object is null";
            } else {
                error = "object " + field + " is null";
            }
            throw ConversionException.fromError(error);
        }
    }

    /**
     * Fetch value from the passed lambda and return value if it is not null
     *
     * @param fieldFetcher - lambda which may return null value or throw NPE
     * @param fieldName    - name of the fetched filed. Name is used for generation of exception message
     * @param <T>          - type of fetched value
     * @return - not null value
     * @throws ConversionException - contains explanation of error with {@code fieldName} inside
     */
    public static <T> T fetchNotNull(Supplier<T> fieldFetcher,
                                     String fieldName) throws ConversionException {
        T t = null;
        try {
            t = fieldFetcher.get();
        } catch (NullPointerException ex) {
            check(null, fieldName);
        }
        check(t, fieldName);
        return t;
    }
}
