package util;


import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Result is a sum type class with meaning of value or error of some computation
 *
 * @param <V> - value type. The presence of the type value means computation is successful
 * @param <E> - error type. The presence of the type value means computation has failed
 */
public class Result<V, E> {

    private V value;
    private E error;

    /**
     * Create a result from value
     */
    public static <V, E> Result fromValue(V value) {
        return new Result<>().setValue(value);
    }

    /**
     * Create a result from error
     */
    public static <V, E> Result fromError(E error) {
        return new Result<>().setError(error);
    }

    /**
     * The method visiting Value/Error pair and invoke provided function
     *
     * @param onValue       - lambda which invokes when the value is present
     * @param errorConsumer - lambda which invokes when the error is present
     * @param <R>           - type of returning value of visiting lambdas
     * @return output of {@code onValue} or {@code errorConsumer}
     */
    public <R> R visit(Function<V, R> onValue, Function<E, R> errorConsumer) {
        if (value != null) {
            return onValue.apply(value);
        }
        if (error != null) {
            errorConsumer.apply(error);
        }
        return null;
    }

    /**
     * The method is a shortcut for visit but only for value matcher
     *
     * @param onValue - lambda which invokes when the value is present
     * @param <R>     - type of returning value of visiting lambdas
     * @return output of {@code onValue} or {@code errorConsumer}
     */
    public <R> R visit(Function<V, R> onValue) {
        return visit(onValue, (e) -> null);
    }

    /**
     * Same as {@link #visit(Function, Function)} but without returning value in lambdas
     */
    public void visitVoid(Consumer<V> onValue, Consumer<E> onError) {
        if (value != null) {
            onValue.accept(value);
        }
        if (error != null) {
            onError.accept(error);
        }
    }

    /**
     * Same as {@link #visitVoid(Consumer, Consumer)} but only for value type
     */
    public void visitVoid(Consumer<V> onValue) {
        visitVoid(onValue, e -> {
        });
    }

    private Result setValue(V value) {
        this.value = value;
        return this;
    }

    private Result setError(E error) {
        this.error = error;
        return this;
    }
}
