package util;

import java.util.Formatter;

/**
 * Class formats provided objects into human-readable strings
 */
public class StringFormatter {
    // formatter
    private Formatter formatter;

    private boolean canChange;
    private boolean needSeparator;

    // TODO: pattern could be extracted and pass as dependency to StringFormatter
    private String keyValuePattern;
    private String singleValuePattern;
    private String collectionItemSeparator;
    private String startPattern;
    private String endPattern;


    /**
     * @param name - identifier of current string
     */
    public StringFormatter(String name) {
        keyValuePattern = "[%s => %s]";
        singleValuePattern = "[%s]";
        collectionItemSeparator = ", ";
        startPattern = "[%s :";
        endPattern = "]";

        formatter = new Formatter();
        canChange = true;
        needSeparator = false;

        formatter.format(startPattern, name);
    }

    public StringFormatter() {
        this("");
    }

    /**
     * Add single value to the output string
     *
     * @param value - value for the insertion into the string
     * @return this
     */
    public <T> StringFormatter append(T value) {
        return append(() -> formatter.format(singleValuePattern, nullable(value)));
    }

    /**
     * Add key/value pair to the output string
     *
     * @return this
     */
    public <K, V> StringFormatter append(K key, V value) {
        return append(() -> formatter.format(keyValuePattern, nullable(key), nullable(value)));
    }

    /**
     * @return output string with all values inside
     */
    public String finish() {
        canChange = false;
        return formatter.format(endPattern).toString();
    }

    /**
     * Provide current state of the output string
     */
    @Override
    public String toString() {
        return formatter.toString();
    }

    private StringFormatter append(Runnable lambda) {
        if (canChange) {
            pushSeparator();
            lambda.run();
        }
        return this;
    }

    private void pushSeparator() {
        if (needSeparator) {
            formatter.format(collectionItemSeparator);
        } else {
            needSeparator = true;
        }
    }

    private static <T> String nullable(T object) {
        if (object == null) {
            return "null";
        } else {
            return object.toString();
        }
    }
}
