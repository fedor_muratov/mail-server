package mail.data;


import util.StringFormatter;

public class MailProviderResponse {

    public enum Status {
        SENT,
        MALFORMED_REQUEST,
        SERVICE_PROBLEM
    }

    private Status status;
    private String errorReason;

    @Override
    public String toString() {
        StringFormatter formatter = new StringFormatter(MailProviderResponse.class.getName());
        switch (status) {
            case SENT:
                formatter.append(status);
                break;
            case MALFORMED_REQUEST:
            case SERVICE_PROBLEM:
                formatter.append(status, errorReason);
                break;
        }
        return formatter.finish();
    }

    public boolean isSuccessful() {
        return status == Status.SENT;
    }

    public Status getStatus() {
        return status;
    }

    public String getErrorReason() {
        return errorReason;
    }

    public static MailProviderResponse success() {
        return new MailProviderResponse();
    }

    public static MailProviderResponse malformedRequest(String reason) {
        return new MailProviderResponse(Status.MALFORMED_REQUEST, reason);
    }

    public static MailProviderResponse mailProviderProblem(String reason) {
        return new MailProviderResponse(Status.SERVICE_PROBLEM, reason);
    }

    private MailProviderResponse(Status status, String reason) {
        this.status = status;
        this.errorReason = reason;
    }

    private MailProviderResponse() {
        status = Status.SENT;
        errorReason = "";
    }
}
