package mail.data.converters;

/**
 * Converter interface.
 * Provides a conversion from one data model to another
 *
 * @param <Source> - type of source object
 * @param <Target> - type of target object
 */
public interface SafeConverter<Source, Target> {
    Target convert(Source sourceObject);
}
