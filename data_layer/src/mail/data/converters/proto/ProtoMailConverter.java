package mail.data.converters.proto;

import com.google.protobuf.ProtocolStringList;
import mail.data.converters.ConversionException;
import mail.data.converters.MailConverter;
import mail.data.nested.Content;
import mail.data.nested.Mail;
import mail.data.nested.MailAddress;
import proto.client_interaction.MailOuterClass;
import util.NotNull;
import util.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ProtoMailConverter extends MailConverter<MailOuterClass.Mail, Mail> {

    private MailOuterClass.Mail protoMail;
    private Mail.MailBuilder builder;

    @Override
    protected void initialize(MailOuterClass.Mail mail) throws ConversionException {
        NotNull.check(mail);
        this.protoMail = mail;
        builder = new Mail.MailBuilder();

    }

    @Override
    protected void fetchFrom() throws ConversionException {
        String protoFrom = NotNull.fetchNotNull(() -> protoMail.getFrom(), "Mail::From");
        MailAddress from = MailAddress.create(protoFrom)
                .orElseThrow(() -> ConversionException.fromError("Bad \"from\" mail address"));
        builder.setFrom(from);
    }

    @Override
    protected void fetchTo() throws ConversionException {
        String protoTo = NotNull.fetchNotNull(() -> protoMail.getTo(), "Mail::To");
        MailAddress to = MailAddress.create(protoTo)
                .orElseThrow(() -> ConversionException.fromError("Bad \"to\" mail address"));
        builder.setTo(to);
    }

    @Override
    protected void fetchCopyTo() throws ConversionException {
        ProtocolStringList copyToList = protoMail.getCopyToList();
        if (copyToList != null) {
            List<MailAddress> cc = new ArrayList<>();
            for (String ccAddress : copyToList) {
                NotNull.check(ccAddress, "Mail::CC");
                MailAddress mailAddress = MailAddress.create(ccAddress)
                        .orElseThrow(() -> ConversionException.fromError("Bad \"cc\" mail"));
                cc.add(mailAddress);
            }
            if (cc.size() > 0) {
                builder.setCC(cc);
            }
        }
    }

    @Override
    protected void fetchContent() throws ConversionException {
        MailOuterClass.Content protoContent = NotNull.fetchNotNull(() -> protoMail.getContent(),
                "Mail::Content");
        Result<Content, ConversionException> result = new ProtoContentConverter().convert(
                protoContent);

        AtomicReference<ConversionException> thrownException = new AtomicReference<>();
        result.visitVoid(content -> builder.setContent(content), thrownException::set);
        if (thrownException.get() != null) {
            throw thrownException.get();
        }

    }

    @Override
    protected void fetchSubject() throws ConversionException {
        String subject = NotNull.fetchNotNull(() -> protoMail.getSubject(), "Mail::Subject");
        builder.setSubject(subject);
    }

    @Override
    protected Mail create() throws ConversionException {
        return builder.build();
    }
}
