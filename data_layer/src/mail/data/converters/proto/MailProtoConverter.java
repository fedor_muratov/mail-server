package mail.data.converters.proto;

import mail.data.converters.ConversionException;
import mail.data.converters.MailConverter;
import mail.data.nested.Content;
import mail.data.nested.Mail;
import mail.data.nested.MailAddress;
import proto.client_interaction.MailOuterClass;
import util.NotNull;

import java.util.List;

public class MailProtoConverter extends MailConverter<Mail, MailOuterClass.Mail> {

    private Mail source;
    private MailOuterClass.Mail.Builder protoBuilder;

    @Override
    protected void initialize(Mail mail) throws ConversionException {
        source = mail;
        protoBuilder = MailOuterClass.Mail.newBuilder();
    }

    @Override
    protected void fetchFrom() throws ConversionException {
        protoBuilder.setFrom(NotNull.fetchNotNull(() -> source.from().plainAddress(),
                "Mail::From"));
    }

    @Override
    protected void fetchTo() throws ConversionException {
        protoBuilder.setTo(NotNull.fetchNotNull(() -> source.to().plainAddress(),
                "Mail::To"));
    }

    @Override
    protected void fetchCopyTo() throws ConversionException {
        List<MailAddress> cc = source.сс();

        if (cc != null) {
            for (MailAddress address : cc) {
                NotNull.check(address);
                protoBuilder.addCopyTo(address.plainAddress());
            }
        }
    }

    @Override
    protected void fetchContent() throws ConversionException {
        MailOuterClass.Content.Builder protoContentBuilder = MailOuterClass.Content.newBuilder();
        protoContentBuilder.setContent(NotNull.fetchNotNull(() -> source.content().data(),
                "Mail::Content::Data"));

        Content.ContentType contentType = NotNull.fetchNotNull(() -> source.content().type(),
                "Mail::Content::Type");
        switch (contentType) {
            case PLAIN_TEXT:
                protoContentBuilder.setType(MailOuterClass.Content.ContentType.PLAIN_TEXT);
                break;
            default:
                throw ConversionException.fromError("Unhandled type " + contentType.name());
        }

        protoBuilder.setContent(protoContentBuilder);
    }

    @Override
    protected void fetchSubject() throws ConversionException {
        protoBuilder.setSubject(NotNull.fetchNotNull(() -> source.subject(),
                "Mail::Subject"));
    }

    @Override
    protected MailOuterClass.Mail create() throws ConversionException {
        return protoBuilder.build();
    }
}
