package mail.data.converters.proto;

import mail.data.MailProviderResponse;
import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import proto.client_interaction.MailApi.ClientSendMailResponse;
import util.Result;

public class ResponseProtoConverter implements Converter<MailProviderResponse, ClientSendMailResponse> {

    private MailProviderResponse sourceObject;
    private ClientSendMailResponse.Builder protoResponseBuilder;

    private void convertStatus() {

        switch (sourceObject.getStatus()) {

            case SENT:
                protoResponseBuilder.setStatus(ClientSendMailResponse.Status.SENT);
                break;
            case MALFORMED_REQUEST:
                protoResponseBuilder.setStatus(ClientSendMailResponse.Status.MALFORMED_REQUEST);
                break;
            case SERVICE_PROBLEM:
                protoResponseBuilder.setStatus(ClientSendMailResponse.Status.SERVICE_PROBLEM);
                break;
        }
    }

    private void convertExplanation() {
        protoResponseBuilder.setExplanation(sourceObject.getErrorReason());
    }

    @Override
    public Result<ClientSendMailResponse, ConversionException> convert(MailProviderResponse sourceObject) {
        this.sourceObject = sourceObject;
        protoResponseBuilder = ClientSendMailResponse.newBuilder();
        convertStatus();
        convertExplanation();
        return Result.fromValue(protoResponseBuilder.build());
    }
}
