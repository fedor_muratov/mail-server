package mail.data.converters.proto;

import mail.data.SendMailRequest;
import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import mail.data.converters.MailConverter;
import mail.data.nested.Mail;
import mail.data.nested.RequestProperties;
import proto.client_interaction.MailApi;
import proto.client_interaction.MailApi.ClientSendMailRequest;
import proto.client_interaction.MailOuterClass;
import util.NotNull;
import util.Result;

import java.util.concurrent.atomic.AtomicReference;

public class ProtoRequestConverter implements Converter<ClientSendMailRequest, SendMailRequest> {
    private MailConverter<MailOuterClass.Mail, Mail> mailConverter;

    private ClientSendMailRequest source;
    private SendMailRequest.RequestBuilder requestBuilder;

    public ProtoRequestConverter() {
        mailConverter = new ProtoMailConverter();
    }

    private void convertMail() throws ConversionException {
        AtomicReference<ConversionException> thrownException = new AtomicReference<>();
        MailOuterClass.Mail email = NotNull.fetchNotNull(() -> source.getEmail(), "Request::Mail");
        mailConverter.convert(email)
                .visitVoid(mail -> requestBuilder = new SendMailRequest.RequestBuilder(mail),
                        thrownException::set);
        if (thrownException.get() != null) {
            throw thrownException.get();
        }
    }

    private void convertRequestProperties() throws ConversionException {
        MailApi.RequestProperties.SendStrategy via = NotNull.fetchNotNull(() -> source.getProperties()
                .getVia(), "Request::Properties::Via");
        RequestProperties.SendingStrategy strategy = null;
        switch (via) {
            case ANY:
                strategy = RequestProperties.SendingStrategy.ANY;
                break;

            case RELIABLE:
                strategy = RequestProperties.SendingStrategy.RELIABLE;
                break;

            case SEND_GRID:
                strategy = RequestProperties.SendingStrategy.SEND_GRID;
                break;

            case UNRECOGNIZED:
                throw ConversionException.fromError("Unrecognized send strategy");
        }
        requestBuilder.setProperties(new RequestProperties(strategy));
    }

    @Override
    public Result<SendMailRequest, ConversionException> convert(ClientSendMailRequest protoRequest) {
        this.source = protoRequest;
        try {
            NotNull.check(this.source, "Request");
            convertMail();
            convertRequestProperties();
            return Result.fromValue(requestBuilder.build());
        } catch (ConversionException ex) {
            return Result.fromError(ex);
        } finally {
            mailConverter = new ProtoMailConverter();
        }
    }
}
