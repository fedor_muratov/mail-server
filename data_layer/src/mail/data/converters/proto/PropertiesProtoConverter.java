package mail.data.converters.proto;

import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import mail.data.nested.RequestProperties;
import proto.client_interaction.MailApi;
import util.NotNull;
import util.Result;

public class PropertiesProtoConverter implements Converter<RequestProperties, MailApi.RequestProperties> {
    private RequestProperties source;
    private MailApi.RequestProperties.Builder protoBuilder;

    private void fetchServiceStrategy() throws ConversionException {
        RequestProperties.SendingStrategy service = source.service();
        switch (service) {
            case ANY:
                protoBuilder.setVia(MailApi.RequestProperties.SendStrategy.ANY);
                break;
            case RELIABLE:
                protoBuilder.setVia(MailApi.RequestProperties.SendStrategy.RELIABLE);
                break;
            case SEND_GRID:
                protoBuilder.setVia(MailApi.RequestProperties.SendStrategy.SEND_GRID);
                break;
            default:
                throw ConversionException.fromError("Missed handler for " + service.name());
        }
    }

    @Override
    public Result<MailApi.RequestProperties, ConversionException> convert(RequestProperties sourceObject) {
        try {
            NotNull.check(sourceObject, "Properties");
            this.source = sourceObject;
            protoBuilder = MailApi.RequestProperties.newBuilder();
            fetchServiceStrategy();
            return Result.fromValue(protoBuilder.build());
        } catch (ConversionException ex) {
            return Result.fromError(ex);
        }
    }
}
