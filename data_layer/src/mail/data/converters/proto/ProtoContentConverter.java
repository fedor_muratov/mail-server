package mail.data.converters.proto;

import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import mail.data.nested.Content;
import proto.client_interaction.MailOuterClass;
import util.NotNull;
import util.Result;

public class ProtoContentConverter implements Converter<MailOuterClass.Content, Content> {
    private MailOuterClass.Content content;
    private Content.ContentBuilder contentBuilder;

    public ProtoContentConverter() {
        contentBuilder = new Content.ContentBuilder();
    }

    private void fetchData() throws ConversionException {
        contentBuilder.setData(NotNull.fetchNotNull(() -> content.getContent(),
                "Content::Content"));
    }

    private void fetchType() throws ConversionException {
        MailOuterClass.Content.ContentType contentType = NotNull.fetchNotNull(() -> content.getType(),
                "Content::Type");
        switch (contentType) {
            case PLAIN_TEXT:
                contentBuilder.setType(Content.ContentType.PLAIN_TEXT);
                break;
            case UNRECOGNIZED:
            default:
                throw ConversionException.fromError("Unrecognized content type");
        }
    }

    @Override
    public Result<Content, ConversionException> convert(MailOuterClass.Content sourceObject) {
        try {
            this.content = sourceObject;
            NotNull.check(sourceObject, "Content");
            fetchData();
            fetchType();
            return Result.fromValue(contentBuilder.build());
        } catch (ConversionException ex) {
            return Result.fromError(ex);
        } finally {
            content = null;
            contentBuilder = new Content.ContentBuilder();
        }
    }
}
