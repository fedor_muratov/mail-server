package mail.data.converters.proto;

import mail.data.SendMailRequest;
import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import mail.data.nested.Mail;
import mail.data.nested.RequestProperties;
import proto.client_interaction.MailApi;
import proto.client_interaction.MailApi.ClientSendMailRequest;
import proto.client_interaction.MailOuterClass;
import util.NotNull;
import util.Result;

import java.util.concurrent.atomic.AtomicReference;

public class RequestProtoConverter implements Converter<SendMailRequest, ClientSendMailRequest> {

    SendMailRequest source;
    ClientSendMailRequest.Builder protoBuilder;
    private Converter<Mail, MailOuterClass.Mail> mailProtoConverter;
    private Converter<RequestProperties, MailApi.RequestProperties> propertiesProtoConverter;

    public RequestProtoConverter() {
        mailProtoConverter = new MailProtoConverter();
        propertiesProtoConverter = new PropertiesProtoConverter();
    }

    private void fetchMail() throws ConversionException {
        Mail mail = NotNull.fetchNotNull(() -> source.mail(), "Request::Mail");

        AtomicReference<ConversionException> ex = new AtomicReference<>();
        mailProtoConverter.convert(mail).visitVoid(protoMail -> {
            protoBuilder.setEmail(protoMail);
        }, ex::set);
        if (ex.get() != null) {
            throw ex.get();
        }

    }

    private void fetchProperties() throws ConversionException {
        RequestProperties properties = NotNull.fetchNotNull(() -> source.properties(),
                "Request::Properties");

        AtomicReference<ConversionException> ex = new AtomicReference<>();
        propertiesProtoConverter.convert(properties).visitVoid(protoProperties -> {
            protoBuilder.setProperties(protoProperties);
        }, ex::set);
        if (ex.get() != null) {
            throw ex.get();
        }
    }

    @Override
    public Result<ClientSendMailRequest, ConversionException> convert(SendMailRequest sourceRequest) {
        try {
            NotNull.check(sourceRequest, "Request");
            source = sourceRequest;
            protoBuilder = ClientSendMailRequest.newBuilder();
            fetchMail();
            fetchProperties();
            return Result.fromValue(protoBuilder.build());
        } catch (ConversionException ex) {
            return Result.fromError(ex);
        }
    }
}
