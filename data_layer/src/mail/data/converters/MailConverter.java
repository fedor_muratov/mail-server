package mail.data.converters;

import util.Result;

/**
 * Conversion backbone for all mail to mail transitions.
 * Class contains steps of the conversion which could be done separately.
 * Implementation should rely only on order of calls for {@link #initialize(Object)} and {@link #create()} methods
 * Transitional steps could be invoked in any order between the pair above.
 */
public abstract class MailConverter<SourceMailObject, TargetMailObject> implements Converter<SourceMailObject, TargetMailObject> {

    protected abstract void initialize(SourceMailObject mail) throws ConversionException;

    protected abstract void fetchFrom() throws ConversionException;

    protected abstract void fetchTo() throws ConversionException;

    protected abstract void fetchCopyTo() throws ConversionException;

    protected abstract void fetchContent() throws ConversionException;

    protected abstract void fetchSubject() throws ConversionException;

    protected abstract TargetMailObject create() throws ConversionException;


    @Override
    public Result<TargetMailObject, ConversionException> convert(SourceMailObject sourceMail) {
        try {
            initialize(sourceMail);
            fetchFrom();
            fetchTo();
            fetchSubject();
            fetchCopyTo();
            fetchContent();
            return Result.fromValue(create());

        }catch (ConversionException ex) {
            return Result.fromError(ex);
        }
    }

}
