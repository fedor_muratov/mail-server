package mail.data.converters;

/**
 * Type of exception which throws on conversion errors
 */
public class ConversionException extends Exception {

    private String reason;

    private ConversionException(String reason) {
        this.reason = reason;
    }

    /**
     * Factory method for creating error
     *
     * @param error - reason of the exception
     */
    public static ConversionException fromError(String error) {
        return new ConversionException(error);
    }

    /**
     * Conversion issue
     */
    public String getReason() {
        return reason;
    }
}
