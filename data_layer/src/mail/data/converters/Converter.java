package mail.data.converters;

import util.Result;

/**
 * Converter interface.
 * Provides a conversion from one data model to another
 *
 * @param <Source> - type of source object
 * @param <Target> - type of target object
 */
public interface Converter<Source, Target> {
    Result<Target, ConversionException> convert(Source sourceObject);
}
