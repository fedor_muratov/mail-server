package mail.data;

import mail.data.nested.Mail;
import mail.data.nested.RequestProperties;

public class SendMailRequest {
    private Mail mail;
    private RequestProperties properties;

    public Mail mail() {
        return mail;
    }

    public RequestProperties properties() {
        return properties;
    }

    private SendMailRequest(Mail mail, RequestProperties properties) {
        this.mail = mail;
        this.properties = properties;
    }

    public static class RequestBuilder {
        private SendMailRequest request;

        public RequestBuilder(Mail mail) {
            this.request = new SendMailRequest(mail,
                    new RequestProperties(RequestProperties.SendingStrategy.ANY));
        }

        public RequestBuilder setProperties(RequestProperties properties) {
            this.request.properties = properties;
            return this;
        }

        public SendMailRequest build() {
            return request;
        }
    }

}
