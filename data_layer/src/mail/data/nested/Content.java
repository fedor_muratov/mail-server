package mail.data.nested;

import util.StringFormatter;

public class Content {

    private String data;
    private ContentType type;

    public enum ContentType {
        PLAIN_TEXT
    }

    public Content(String data, ContentType type) {
        this.data = data;
        this.type = type;
    }

    public static Content empty() {
        return new Content(" ", ContentType.PLAIN_TEXT);
    }

    @Override
    public String toString() {
        return new StringFormatter("Content").append("type", type).append("data", data).finish();
    }

    public String data() {
        return data;
    }

    public ContentType type() {
        return type;
    }

    public static class ContentBuilder {
        Content content;

        public ContentBuilder() {
            content = Content.empty();
        }

        public ContentBuilder setData(String data) {
            content.data = data;
            return this;
        }

        public ContentBuilder setType(ContentType type) {
            content.type = type;
            return this;
        }

        public Content build() {
            return content;
        }

    }
}
