package mail.data.nested;

import util.StringFormatter;

import java.util.ArrayList;
import java.util.List;

public class Mail {
    private MailAddress from;
    private MailAddress to;
    private List<MailAddress> copyTo;
    private Content content;
    private String subject;

    private Mail(MailAddress from, MailAddress to) {
        this();
        this.from = from;
        this.to = to;

    }

    private Mail() {
        copyTo = new ArrayList<>();
        content = Content.empty();
    }

    public MailAddress from() {
        return from;
    }

    public MailAddress to() {
        return to;
    }

    // todo make collection immutable
    public List<MailAddress> сс() {
        return copyTo;
    }

    public Content content() {
        return content;
    }

    public String subject() {
        return subject;
    }

    @Override
    public String toString() {
        return new StringFormatter().append(from)
                .append(to)
                .append("cc", copyTo)
                .append(content)
                .finish();
    }

    public static class MailBuilder {
        private Mail mail;

        private boolean setFromInvoked;
        private boolean setToInvoked;

        public MailBuilder(MailAddress from, MailAddress to) {
            this();
            setFrom(from).setTo(to);
        }

        public MailBuilder() {
            mail = new Mail();
            setFromInvoked = false;
            setToInvoked = true;
        }

        public MailBuilder setFrom(MailAddress from) {
            mail.from = from;
            setFromInvoked = true;
            return this;
        }

        public MailBuilder setTo(MailAddress to) {
            mail.to = to;
            setToInvoked = true;
            return this;
        }

        public MailBuilder setCC(List<MailAddress> copyTo) {
            mail.copyTo = new ArrayList<>(copyTo);
            return this;
        }

        public MailBuilder setContent(Content content) {
            mail.content = content;
            return this;
        }

        public MailBuilder setSubject(String subject) {
            mail.subject = subject;
            return this;
        }

        public Mail build() {
            if (setFromInvoked && setToInvoked) {
                return mail;
            } else {
                return null;
            }
        }

    }


}
