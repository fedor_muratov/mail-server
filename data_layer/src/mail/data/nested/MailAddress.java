package mail.data.nested;

import util.StringFormatter;

import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;

public class MailAddress {
    private final String address;
    private static MailValidator mailValidator = new MailValidator();

    private MailAddress(String address) {
        this.address = address;
    }

    public String plainAddress() {
        return address;
    }

    @Override
    public String toString() {
        return new StringFormatter().append("address", address).finish();
    }

    public static Optional<MailAddress> create(String address) {
        if (mailValidator.valid(address)) {
            return Optional.of(new MailAddress(address));
        } else {
            return Optional.empty();
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailAddress that = (MailAddress) o;
        return address.equals(that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }

    private static class MailValidator {


        private final String emailRegex;
        private final Pattern pattern;

        MailValidator() {
            emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\. [a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
            pattern = Pattern.compile(emailRegex);
        }


        public boolean valid(String mailAddress) {
            if (mailAddress == null) {
                return false;
            }

            return pattern.matcher(mailAddress).matches();
        }
    }
}
