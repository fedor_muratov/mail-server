package mail.data.nested;

import java.util.Objects;

public class RequestProperties {
    private SendingStrategy service;

    public RequestProperties(SendingStrategy service) {
        this.service = service;
    }

    public SendingStrategy service() {
        return service;
    }

    public enum SendingStrategy {
        ANY,
        RELIABLE,
        SEND_GRID,
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RequestProperties that = (RequestProperties) o;
        return service == that.service;
    }

    @Override
    public int hashCode() {
        return Objects.hash(service);
    }
}
