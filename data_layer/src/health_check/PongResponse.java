package health_check;

import util.StringFormatter;

public class PongResponse {
    private final long uptime;

    public PongResponse(long uptime) {
        this.uptime = uptime;
    }

    public long uptime() {
        return uptime;
    }

    @Override
    public String toString() {
        return new StringFormatter("PongResponse").append("uptime", uptime).toString();
    }
}
