package health_check.converters;

import health_check.PongResponse;
import mail.data.converters.ConversionException;
import mail.data.converters.Converter;
import proto.health_check.HealthcheckApi;
import util.NotNull;
import util.Result;

public class ProtoPongResponseConverter implements Converter<HealthcheckApi.PongResponse, PongResponse> {

    @Override
    public Result<PongResponse, ConversionException> convert(HealthcheckApi.PongResponse protoObject) {
        try {
            long uptime = NotNull.fetchNotNull(() -> protoObject.getUptime(),
                    "PongResponse::Uptime");
            return Result.fromValue(new PongResponse(uptime));
        } catch (ConversionException ex) {
            return Result.fromError(ex);
        }
    }
}
