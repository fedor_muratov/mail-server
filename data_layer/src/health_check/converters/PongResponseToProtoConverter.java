package health_check.converters;

import health_check.PongResponse;
import mail.data.converters.SafeConverter;
import proto.health_check.HealthcheckApi;

public class PongResponseToProtoConverter implements SafeConverter<PongResponse, HealthcheckApi.PongResponse> {
    @Override
    public HealthcheckApi.PongResponse convert(PongResponse sourceObject) {
        return HealthcheckApi.PongResponse.newBuilder().setUptime(sourceObject.uptime()).build();
    }
}
