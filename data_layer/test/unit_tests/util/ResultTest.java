package util;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class ResultTest {
    class V1 {
    }

    class E1 {
    }

    private <V, E> void assertValue(Result<V, E> result) {
        final boolean[] isInvoked = {false};
        result.visit(v -> {
            isInvoked[0] = true;
            return null;
        }, e -> null);

        assertTrue(isInvoked[0]);
    }

    private <V, E> void assertError(Result<V, E> result) {
        final boolean[] isInvoked = {false};
        result.visit(v -> null, e -> {
            isInvoked[0] = true;
            return null;
        });

        assertTrue(isInvoked[0]);
    }

    @Test
    public void testApplyValue() {
        Result<V1, E1> result = Result.<V1, E1>fromValue(new V1());
        assertValue(result);
    }

    @Test
    public void testApplyError() {
        Result<V1, E1> result = Result.<V1, E1>fromError(new E1());
        assertError(result);
    }

}
