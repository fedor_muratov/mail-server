package util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringFormatterTest {
    @Test
    public void testStringFormatter() {
        StringFormatter stringFormatter = new StringFormatter("name");
        stringFormatter.append("key", "value").append("only_key");
        assertEquals("[name :[key => value], [only_key]]", stringFormatter.finish());
    }
}
