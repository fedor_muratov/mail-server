package mail.converters;

import mail.data.SendMailRequest;
import mail.data.converters.Converter;
import mail.data.converters.proto.ProtoRequestConverter;
import org.junit.Before;
import org.junit.Test;
import proto.client_interaction.MailApi;
import proto.client_interaction.MailApi.ClientSendMailRequest;
import proto.client_interaction.MailApi.RequestProperties;
import proto.client_interaction.MailOuterClass;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class ProtoRequestConverterTest {

    private Converter<MailApi.ClientSendMailRequest, SendMailRequest> converter;

    @Before
    public void setUp() {
        converter = new ProtoRequestConverter();
    }

    /**
     * @given initialized proto converter for requests
     * @when valid input request is provided
     * @then valid data message is returned
     */
    @Test
    public void testValidInput() {


        MailApi.RequestProperties.Builder properties =
                MailApi.RequestProperties.newBuilder()
                        .setVia(MailApi.RequestProperties.SendStrategy.ANY);


        MailOuterClass.Mail.Builder mail = MailOuterClass.Mail.newBuilder()
                .setFrom("foo@example.com")
                .setTo("bar@example.com")
                .setSubject("My proto mail")
                .setContent(MailOuterClass.Content.newBuilder()
                        .setType(MailOuterClass.Content.ContentType.PLAIN_TEXT)
                        .setContent("Here is the text"));

        MailApi.ClientSendMailRequest request = MailApi.ClientSendMailRequest.newBuilder()
                .setProperties(properties)
                .setEmail(mail)
                .build();

        converter.convert(request).visitVoid(r -> {
            assertEquals("foo@example.com", request.getEmail().getFrom());
            assertEquals("bar@example.com", request.getEmail().getTo());
            assertEquals("My proto mail", request.getEmail().getSubject());
            assertEquals("Here is the text", request.getEmail().getContent().getContent());
            assertEquals(MailOuterClass.Content.ContentType.PLAIN_TEXT,
                    request.getEmail().getContent().getType());
            assertEquals(MailApi.RequestProperties.SendStrategy.ANY,
                    request.getProperties().getVia());
        }, e -> fail("Conversion from proto to mail has failed: " + e.getReason()));
    }

    /**
     * @given initialized proto converter for requests
     * @when invalid input request is provided - missed email
     * @then error is occurred
     */
    @Test
    public void testInvalidOutputMissedEmail() {

        RequestProperties.Builder properties =
                RequestProperties.newBuilder()
                        .setVia(RequestProperties.SendStrategy.ANY);


        ClientSendMailRequest request = ClientSendMailRequest.newBuilder()
                .setProperties(properties)
                .build();

        converter.convert(request).visitVoid(r -> fail("Conversion should fail"));
    }
}
