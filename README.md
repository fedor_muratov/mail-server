# Mail Server
The project provides a proxy for mailing using Protobuf for client interaction.
## Available features

### Supported mail services:
* SendGrid
The current version contains only one mail services, but the architecture provides an easy way to extend it via implementation corresponding interface.

### Sending strategies
The project supports different strategies for sending, which may be suitable in different situations.
#### Sending methods
The mail can be sent via the following methods:
* `sendMail`
It is a blocking method which returns the status of delivery.
* `sendMailAsync`
It is a non-blocking method without status. The method is useful for testing purposes first: load and performance testing and in scenarios where don't require additional strategies to send an e-mail.
#### Sending parameters
Every sending method has a sending parameter. This parameter provides a sending strategy.
* ANY strategy
The mail server picks a random provider from available and sends the mail via this server. This strategy is useful in scenarios with changing the profile of load and works as a primitive load balancer.
* RELIABLE strategy
Use only SendGrid service for sending. This strategy may be useful in situations where we want to control the limits of data sent for different mail services.
* SEND_GRID strategy
Send an e-mail via SendGrid service only.

### Reconnection
The server will try to make a new connection if mail sending is failed. This feature is useful in environments with an unstable connection.

### Healthcheck
The project has ping request for checking availability. This service is useful in clusters with automatic deployments and monitoring.

### Performance metrics
The project has particular log messages with a performance tag. Those metrics can be useful during performance/load testing and in deployed systems for measuring the latency and throughput of requests from real clients.

### Additional
The project has a simple CLI for checking the functionality.

## Quick start
The section contains the main usage scenarios of the project.

### Build
Build the project:
```sh
gradle build
```

### Server
Server requires API key of SendGrid. Setup environemnt variable `SENDGRID_API_KEY`:
```sh
export SENDGRID_API_KEY="<your_api_key>"  
```
Start server:
```sh
gradle :server:run
```

### Client
Starting the client.
Available arguments:
`-host` host of the server
`-port` port of the server
Next banch of parameters is optional
`-async=[true|false]` should client wait for the response
`-request=[SEND_GRID|ANY|RELIABLE]` type of sending strategy

Example of the usage
```sh
gradle :client:run --args='-host=localhost -port=50666 -async=false -request=RELIABLE'
```

## Architecture
Implementation is written in Java8 + Gradle. Interaction with clients is based on GRPC.
The project has 3 subprojects:
* data_layer
Data layer contains proto-files and Java data model classes. Data model classes are server-level objects which can be useful on both sides: a client has the same restrictions in object creation as server parser, the server has unified model for working with all types of mail services.

* client
Client project contains simple CLI. It is possible to share the e-mail with different sending strategies.

* server
Server project contains the code which sends mail using corrsesponding mail provider.

### Server in deep
The main logic of mail performing contains in ClientService::performRequest. 
The pipeline of the mail(in production use case) is the following:
1. Proto-mail converts to data layer mail;
2. Mail pushes into ConcurrentMailSender::send where it exectues on thread pool as a sending task;
3. Perform sending of the mail based on provided stategy;
4. returns the result of the excecution if required.

Implementation of new mail service requires the following steps:
* implement MailServiceProvider, which contains _blocking_ interface for the sending
* _optional_ inherit from MailServiceReconnectionWrapper if reconnection to the service is required
* add service to ComonentsInitialization
* _optional_ add service to cleint's API and add conversion in data_layer RequestProperties

The project is written is the exception-free approach where expected errors(such as conversion, connection, etc.) are handled explicitly. For this approach was written a Result class, which is a sum type of Value and Error.


## Technical debt
* Currenty sendMailAsync method returns `void`(`google.protobuf.Empty`) but it could be changed with identifier of the status.
* There is no way to set up a fixed port for GRPC server. It must be done before the production.
* Project contains few todos which should be fixed before the release.
* CLI hasn't mail creator.
* possibility of MITM attack because GRPC and SendGrid sender don't use an encription.

## Possible improvements
* Add Docker image with the project and dependencies for deployment in production.
* Rework blocking sending with NIO in async case.
* Missed automatic system test for sending a mail via CLI.
* Used a fixed thread pool in ComponentsInitialization. A number of thread could be tuning regarding the number of cores on the hosing machine.
* Add integration tests for server initialization.
* Add performance/soak tests.
* Data_layer contains a boiler-plate code for conversion to proto-model and vise versa. Probably, it could be done with automatic conversion.
* Add attachments to e-mails.
* Add batches of e-mails for the sending.
